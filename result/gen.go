// Thank robots for this file that was generated for you at 2020-07-05 18:16:56.626712397 +0300 MSK m=+0.009667573
package result

import (
	"log"
	"os"
	"strconv"
)

type TypeIn struct {
	Sum int `json:"sum"`
}

type TypeOut struct {
	Result int `json:"result"`
}

func Handle(input TypeIn) TypeOut {
	var out TypeOut

	rawMultiplicator := os.Getenv("__ANZ_MULTIPLY")
	multiplicator, err := strconv.Atoi(rawMultiplicator)
	if err != nil {
		log.Printf("error parsing __ANZ_MULTIPLY into int: %q", err)
		return out
	}
	out.Result = input.Sum * multiplicator
	return out
}
